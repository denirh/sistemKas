<?php 

  require_once "core/init.php";

  $error = '';

  if (isset($_SESSION['user'])) {
        header('Location:index.php');
  }

  if (isset($_POST['submit'])) {
    $nama = $_POST['username'];
    $pass = $_POST['password'];

    if (register_nama($nama)){  
      if (!empty(trim($nama)) && !empty(trim($pass))) {
        // memasukan data ke database
        if (register_user($nama, $pass)){
          header('Location:index.php');
        }else{
          $error = "gagal";
        }
      }else{
        $error ="tidak boleh kosong";
      }
    }else{
      $error = "nama sudah ada";
    }
  }

?>


<!DOCTYPE html>
<html>
<head>
  <title>LOGIN PAGE</title>
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
  <style type="text/css">
    *{
      font-family: 'Roboto', sans-serif;
      margin: 0;
      padding: 0;
    }
  </style>
</head>
<body style="background-color: #00bcd4 !important;">
    <div id="wrapper-log-in" style="background-color: #fff; width: 20%; margin: 0 auto; border-radius: 3px; margin-top: 80px;" >
      <div id="login-page" style="padding: 40px 20px;">
        <img src="assets/img/logo.png" style="width: 110px; height: 110px; display: block; margin: 0 auto; margin-bottom: 25px;">
        <p style="font-weight: 400; margin: 0; text-align: center;">SILAHKAN DAFTAR DULU AGAR BISA MASUK KE SISTEM</p>
        <form action="register.php" method="post" style="margin-top: 30px;">
          <input type="text" name="username" style="width: 100%; padding: 5px; margin-top: 10px; border: none; border-bottom: 1px solid #00bcd4;" placeholder="Masukkan Nama"><br><br>
          <input type="password" name="password" style="width: 100%; padding: 5px; margin-top: 10px; border: none; border-bottom: 1px solid #00bcd4;" placeholder="Password">
          <input type="submit" name="submit" value="DAFTAR" style="width: 100%; border: 0; padding: 8px; color: #fff; background-color: #ff4081; border-radius: 0; margin: 25px 0;">
        </form>
        <a href="login.php" class="btn-register" style="color: #00bcd4;">Masuk ?</a>
        <?php if ( $error != '' ) { ?>
          <div id="error" style="background-color: red; color: #fff; padding: 5px 10px; margin-top: 5px;">
            <?= $error; ?>
          </div>
        <?php } ?>
      </div>
    </div>
</body>
</html>
