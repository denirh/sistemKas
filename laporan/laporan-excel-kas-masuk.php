<?php 
	error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
	$konek = mysqli_connect("localhost", "root", "", "kasphp");
	$filename = "Laporan Kas Masuk";
	header("Content-Type: application/vnd.ms-excel");    
	header("Content-Disposition: attachment; filename=$filename.xls"); 
?>

<h2>LAPORAN KAS MASUK</h2>
	<table border="1" >
      	<thead>
            <tr>
                <th style="border-collapse: collapse; padding: 8px 12px">No</th>
                <th style="border-collapse: collapse; padding: 8px 12px">Kode</th>
                <th style="border-collapse: collapse; padding: 8px 12px">Tanggal</th>
                <th style="border-collapse: collapse; padding: 8px 12px">Ketarangan</th>
                <th style="border-collapse: collapse; padding: 8px 12px">Jumlah (Rp)</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $no = 1;
                $data = mysqli_query($konek, "SELECT * FROM kas WHERE jenis = 'Masuk' ");
                        while( $output = mysqli_fetch_assoc($data) ){ 
            ?>
                       
            <tr class="odd gradeX">
                <td style="border-collapse: collapse; padding: 8px 12px"><?php echo $no++; ?></td>
                <td style="border-collapse: collapse; padding: 8px 12px"><?php echo $output['kode']; ?></td>
                <td style="border-collapse: collapse; padding: 8px 12px"><?php echo date('d F Y', strtotime($output['tanggal'])); ?></td>
                <td style="border-collapse: collapse; padding: 8px 12px"><?php echo $output['keterangan']; ?></td>
                <td align="right" style="border-collapse: collapse; padding: 8px 12px"><?php echo number_format( $output['jumlah'] ).",-"; ?></td>
            </tr>

            <?php 
                $total = $total+$output['jumlah'];
            } ?>
        </tbody>
            <tr>
            	<th colspan="4" class="text-center" style="font-size: 17px;">Jumlah Kas Masuk</th>
            	<th align="right" style="font-size: 17px;"><?php echo "Rp. ". number_format($total).",-"; ?></th>
            </tr>
    </table>