<?php 
	error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
	$konek = mysqli_connect("localhost", "root", "", "kasphp");
	$filename = "Laporan Kas Keluar";
	header("Content-Type: application/vnd.ms-excel");    
	header("Content-Disposition: attachment; filename=$filename.xls"); 
?>

<h2>REKAPITULASI KAS</h2>
	<table border="1" >
      	<thead>
            <tr>
                <th style="border-collapse: collapse; padding: 8px 12px">No</th>
                <th style="border-collapse: collapse; padding: 8px 12px">Kode</th>
                <th style="border-collapse: collapse; padding: 8px 12px">Tanggal</th>
                <th style="border-collapse: collapse; padding: 8px 12px">Ketarangan</th>
                <th style="border-collapse: collapse; padding: 8px 12px">Masuk</th>
                <th style="border-collapse: collapse; padding: 8px 12px">Jenis</th>
                <th style="border-collapse: collapse; padding: 8px 12px">Keluar (Rp)</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $no = 1;
                $data = mysqli_query($konek, "SELECT * FROM kas ");
                        while( $output = mysqli_fetch_array($data) ){ 
            ?>
                       
            <tr class="odd gradeX">
                <td style="border-collapse: collapse; padding: 8px 12px"><?php echo $no++; ?></td>
                <td style="border-collapse: collapse; padding: 8px 12px"><?php echo $output['kode']; ?></td>
                <td style="border-collapse: collapse; padding: 8px 12px"><?php echo date('d F Y', strtotime($output['tanggal'])); ?></td>
                <td style="border-collapse: collapse; padding: 8px 12px"><?php echo $output['keterangan']; ?></td>
                <td style="border-collapse: collapse; padding: 8px 12px"><?php echo $output['jumlah']; ?></td>
                <td style="border-collapse: collapse; padding: 8px 12px"><?php echo $output['jenis']; ?></td>
                <td align="right" style="border-collapse: collapse; padding: 8px 12px"><?php echo number_format( $output['keluar'] ).",-"; ?></td>
            </tr>

            <?php 
                $total = $total+$output['jumlah'];
                $total_keluar = $total_keluar+$output['keluar'];
                $saldo = $total - $total_keluar;

                } 
            ?>
        </tbody>
            <tr>
                <th colspan="5" class="text-center" style="font-size: 17px;">Jumlah Kas Masuk</th>
                <th align="right" style="font-size: 17px;"><?php echo number_format($total).",-"; ?></th>
            </tr>
            <tr>
                <th colspan="5" class="text-center" style="font-size: 17px;">Jumlah Kas Keluar</th>
                <th align="right" style="font-size: 17px;"><?php echo number_format($total_keluar).",-"; ?></th>
            </tr>
            <tr>
                <th colspan="5" class="text-center" style="font-size: 17px;">Saldo Tersisa</th>
                <th align="right" style="font-size: 17px;"><?php echo "Rp. ". number_format($saldo).",-"; ?></th>
            </tr>
</table>