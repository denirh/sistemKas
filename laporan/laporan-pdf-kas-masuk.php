<?php 
	error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
	$konek = mysqli_connect("localhost", "root", "", "kasphp");
	$content = '
				<style type="text/css">
					.tabel{border-collapse: collapse;}
					th{padding: 8px 12px; background-color: #eee;}
					td{padding: 8px 12px;}
				</style>';

	$content .= '
				<page>
					<h1>LAPORAN KAS MASUK</h1>
					<br>
					<table border="1" class="tabel">
						<tr>
							<th>No</th>
                            <th>Kode</th>
                            <th>Tanggal</th>
                            <th>Ketarangan</th>
                            <th>Jumlah (Rp)</th>
						</tr>';
						$no = 1;
		                $data = mysqli_query($konek, "SELECT * FROM kas WHERE jenis = 'Masuk' ");
		                        while( $output = mysqli_fetch_assoc($data) ){
					$content .='
						<tr>
			                <td> '.$no++.' </td>
			                <td> '.$output['kode'].' </td>
			                <td> '.date('d F Y', strtotime($output['tanggal'])).' </td>
			                <td> '.$output['keterangan'].' </td>
			                <td align="right"> '.number_format( $output["jumlah"] ).' </td>
			            </tr>';
			            $total = $total+$output['jumlah'];
			        }

			        $content .='
							<tr>
				            	<th colspan="4" class="text-center" style="font-size: 17px;">Jumlah Kas Masuk</th>
				            	<th align="right" style="font-size: 17px;">Rp. '.number_format($total).',- </th>
				            </tr>
					</table>
				</page>';

	 require_once ("../assets/html2pdf/html2pdf.class.php");
	 $html2pdf = new HTML2PDF ("P", "A4", "fr");
	 $html2pdf->WriteHTML($content);
	 $html2pdf->Output("test.pdf");


?>