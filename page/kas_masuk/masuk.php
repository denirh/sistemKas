<div class="row">
    <div class="col-md-12">
        <div class="halaman">
            <h2 class="text-center">DATA PEMASUKAN KAS</h2>
            <hr>
        </div>
        <!-- Advanced Tables -->
        <div class="panel panel-primary">
            <div class="panel-heading">
                Data Kas Masuk EDIPENI
            </div>
        <div class="panel-body">
            <div class="table-responsive">
                <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid">
                    <table id="dataTables-example" class="table table-striped table-bordered table-hover dataTable"  aria-describedby="dataTables-example_info">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode</th>
                                <th>Tanggal</th>
                                <th>Ketarangan</th>
                                <th>Jumlah (Rp)</th>
                                <th>Pilihan</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php 
                                $no = 1;

                                require_once "function/db.php";
                                $data = mysqli_query($link, "SELECT * FROM kas WHERE jenis = 'Masuk' ");
                                    while( $output = mysqli_fetch_array($data) ){ ?>
                        
                            <tr class="odd gradeX">
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $output['kode']; ?></td>
                                <td><?php echo date('d F Y', strtotime($output['tanggal'])); ?></td>
                                <td><?php echo $output['keterangan']; ?></td>
                                <td align="right"><?php echo number_format( $output['jumlah'] ).",-"; ?></td>
                                <td class="text-center">
                                    <a href="edit-data.php" class="btn btn-info" id="edit_data" data-toggle="modal" data-target="#edit" 
                                       data-id="<?php echo $output['kode']; ?>"
                                       data-tanggal="<?php echo $output['tanggal']; ?>"
                                       data-keterangan="<?php echo $output['keterangan']; ?>"
                                       data-jumlah="<?php echo $output['jumlah']; ?>" >

                                        <i class="fa fa-edit"></i> Ubah
                                    </a>
                                    <a onclick="return confirm('Yakin Mau Menghapus ?')" href="?page=masuk&aksi=hapus&id=<?php echo $output['kode']; ?>" class="btn btn-danger">
                                        <i class="fa fa-trash"></i> Hapus
                                    </a>
                                </td>
                            </tr>

                            <?php 
                                $total = $total+$output['jumlah'];
                                } 
                            ?>
                        </tbody>
                            <tr>
                                <th colspan="4" class="text-center" style="font-size: 17px;">Jumlah Kas Masuk</th>
                                <th class="text-right" style="font-size: 17px;"><?php echo "Rp. ". number_format($total).",-"; ?></th>
                            </tr>
                    </table>

                    <!--  Modals dan tambah data -->
                        <div class="panel-body">
                            <a class="btn btn-add " data-toggle="modal" data-target="#myModal">
                              <i class="fa fa-plus"></i> Tambah Data</a>
                            <a href="./laporan/laporan-excel-kas-masuk.php" target="blank" class="btn btn-info btn" ><i class="fa fa-print"></i> Export Ke Excel</a>
                            <a href="./laporan/laporan-pdf-kas-masuk.php" target="blank" class="btn btn-info btn" ><i class="fa fa-print"></i> Export Ke PDF</a>
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Isi Form Dibawah Untuk Menambah Data</h4>
                                        </div>
                                        <div class="modal-body">                                            
                                            <form action="" method="post">
                                              <div class="form-group create-data">
                                                <label>Kode</label>
                                                <input class="form-control data-input" name="kode" placeholder="Masukkan Kode">
                                              </div>
                                              <div class="form-group create-data">
                                                <label>Keterangan</label>
                                                <textarea class="form-control data-input" rows="3" name="ket"></textarea>
                                              </div>
                                              <div class="form-group create-data">
                                                <label>Tanggal</label>
                                                <input class="form-control data-input" type="date" name="tanggal" >
                                              </div>                                              
                                              <div class="form-group create-data">
                                                <label>Jumlah</label>
                                                <input class="form-control data-input" name="jumlah" type="number">
                                              </div>
                                              </div>
                                              <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                                <input type="submit" name="simpan" value="Simpan Data" class="btn btn-primary">
                                              </div>
                                            </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                     <!-- End Modals-->

                    <?php 

                        if ( isset($_POST['simpan']) ) {
                            
                            $kode    = $_POST['kode'];
                            $tanggal = $_POST['tanggal'];
                            $ket     = $_POST['ket'];
                            $jumlah  = $_POST['jumlah'];

                            $query = mysqli_query($link, "INSERT INTO kas (kode, keterangan, tanggal, jumlah, jenis, keluar) VALUES ('$kode', '$ket','$tanggal', '$jumlah', 'Masuk', 0) ");
                            if ($query) { 
                    ?>             
                                <script>
                                    alert("Berhasil Ditambahkan");
                                    window.location.href="?page=masuk";
                                </script>

                    <?php 
                            }
                        }
                    ?>

                    <!-- Akhir halaman tambah data -->
                    <!-- Modals dan Halaman edit data -->
                        <div class="panel-body">
                            <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Silahkan Mengubah Data</h4>
                                        </div>
                                        <div class="modal-body" id="modal_edit">                                            
                                            <form action="" method="post">
                                              <div class="form-group create-data">
                                                <label>Kode</label>
                                                <input class="form-control data-input" name="kode" placeholder="Masukkan Kode" id="kode" readonly>
                                              </div>
                                              <div class="form-group create-data">
                                                <label>Keterangan</label>
                                                <textarea class="form-control data-input" rows="3" name="ket" id="keterangan"></textarea>
                                              </div>
                                              <div class="form-group create-data">
                                                <label>Tanggal</label>
                                                <input class="form-control data-input" type="date" name="tanggal" id="tanggal">
                                              </div>                                              
                                              <div class="form-group create-data">
                                                <label>Jumlah</label>
                                                <input class="form-control data-input" name="jumlah" type="number" id="jumlah">
                                              </div>
                                              </div>
                                              <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                                <input type="submit" name="ubah" value="Ubah Data" class="btn btn-primary">
                                              </div>
                                            </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                     <!-- End Modals-->
                     <script src="assets/js/jquery-1.10.2.js"></script>
                     <script type="text/javascript">
                         $(document).on("click", "#edit_data", function(){
                            var kode       = $(this).data('id');
                            var tanggal    = $(this).data('tanggal');
                            var keterangan = $(this).data('keterangan');
                            var jumlah     = $(this).data('jumlah');

                            $("#modal_edit #kode").val(kode);
                            $("#modal_edit #keterangan").val(keterangan);
                            $("#modal_edit #tanggal").val(tanggal);
                            $("#modal_edit #jumlah").val(jumlah);
                         })
                     </script>
                     <?php                         

                        if (isset($_POST['ubah'])) {
                            $kode    = $_POST['kode'];
                            $tanggal = $_POST['tanggal'];
                            $ket     = $_POST['ket'];
                            $jumlah  = $_POST['jumlah'];

                            $query = mysqli_query($link, "UPDATE kas SET keterangan = '$ket',
                                                             tanggal    = '$tanggal',
                                                             jumlah     = '$jumlah',
                                                             jenis      = 'Masuk',
                                                             keluar     = 0
                                              WHERE kode = '$kode' ");
                            if ($query) { 
                    ?>             
                                <script>
                                    alert("Ubah Data Berhasil");
                                    window.location.href="?page=masuk";
                                </script>

                    <?php }} ?>
                    <!-- Halaman edit data -->
                </div>
            </div>
        </div>
    </div>
</div>