<div class="row">
    <div class="col-md-12">
        <div class="halaman">
            <h2 class="text-center">REKAPITULASI KAS</h2>
            <hr>
        </div>
        <!-- Advanced Tables -->
        <div class="panel panel-primary">
            <div class="panel-heading">
                Data Rekapitulasi Kas EDIPENI
            </div>
        <div class="panel-body">
            <div class="table-responsive">
                <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid">
                    <table id="dataTables-example" class="table table-striped table-bordered table-hover dataTable"  aria-describedby="dataTables-example_info">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode</th>
                                <th>Tanggal</th>
                                <th>Ketarangan</th>
                                <th>Masuk (Rp)</th>
                                <th>Jenis</th>
                                <th>Keluar (Rp)</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php 
                                $no = 1;

                                require_once "function/db.php";
                                $data = mysqli_query($link, "SELECT * FROM kas");
                                    while( $output = mysqli_fetch_array($data) ){ ?>
                        
                            <tr class="odd gradeX">
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $output['kode']; ?></td>
                                <td><?php echo date('d F Y', strtotime($output['tanggal'])); ?></td>
                                <td><?php echo $output['keterangan']; ?></td>
                                <td align="right"><?php echo number_format( $output['jumlah'] ).",-"; ?></td>
                                <td><?php echo $output['jenis']; ?></td>
                                <td align="right"><?php echo number_format( $output['keluar'] ).",-"; ?></td>                                
                            </tr>

                            <?php 
                                $total = $total+$output['jumlah'];
                                $total_keluar = $total_keluar+$output['keluar'];
                                $saldo = $total - $total_keluar;

                                } 
                            ?>
                        </tbody>
                            <tr>
                                <th colspan="5" class="text-center" style="font-size: 17px;">Jumlah Kas Masuk</th>
                                <th class="text-right" style="font-size: 17px;"><?php echo number_format($total).",-"; ?></th>
                            </tr>
                            <tr>
                                <th colspan="5" class="text-center" style="font-size: 17px;">Jumlah Kas Keluar</th>
                                <th class="text-right" style="font-size: 17px;"><?php echo number_format($total_keluar).",-"; ?></th>
                            </tr>
                            <tr>
                                <th colspan="5" class="text-center" style="font-size: 17px;">Saldo Tersisa</th>
                                <th class="text-right" style="font-size: 17px;"><?php echo "Rp. ". number_format($saldo).",-"; ?></th>
                            </tr>
                    </table>
                    <a href="./laporan/laporan-excel-rekap.php" target="blank" class="btn btn-info btn" ><i class="fa fa-print"></i> Export Ke Excel</a>
                    <a href="./laporan/laporan-pdf-rekap.php" target="blank" class="btn btn-info btn" ><i class="fa fa-print"></i> Export Ke PDF</a>
                </div>
            </div>
        </div>
    </div>
</div>