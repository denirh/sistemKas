<div class="row">
    <div class="col-md-12">
        <div class="halaman">
            <h2 class="text-center">DAFTAR PENGGUNA SISTEM</h2>
            <hr>
        </div>
        <!-- Advanced Tables -->
        <div class="panel panel-primary">
            <div class="panel-heading">
                Data Pengguna Sistem Kas EDIPENI
            </div>
        <div class="panel-body">
            <div class="table-responsive">
                <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid">
                    <table id="dataTables-example" class="table table-striped table-bordered table-hover dataTable"  aria-describedby="dataTables-example_info">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Pengguna</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php 
                                $no = 1;

                                require_once "core/init.php";
                                $data = mysqli_query($link, "SELECT * FROM kas_users");
                                    while( $output = mysqli_fetch_array($data) ){ ?>

                        
                            <tr class="odd gradeX">
                                <td><?php echo $no++; ?></td>
                                <td style="text-transform: uppercase;"><?php echo $output['username']; ?></td>                               
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>