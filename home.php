<?php 
    $query = mysqli_query($link, "SELECT * FROM kas");

    while ( $print = mysqli_fetch_assoc($query) ) {
        $total            = $print['jumlah'];
        $output_kas_masuk = $output_kas_masuk + $total;

        $total_keluar      = $print['keluar'];
        $output_kas_keluar = $output_kas_keluar + $total_keluar;

        $saldo = $output_kas_masuk - $output_kas_keluar;
    }

?>
<marquee style="color: #673ab8;">SELAMAT DATANG DI SISTEM INFORMASI PENGELOLAAN DANA KAS PANITIA HALAL BI HALAL 2018 DUSUN NGROMBO-PEPET</marquee>
<div id="page-inner">
    <div class="row">
        <div class="col-md-12">
            <h2 style="font-weight: 700; color: #251e39; "><span class="fa fa-university"></span> HALAMAN UTAMA </h2>   
            <h3 style="font-weight: 700; color: #673ab8; ">Sistem Pengelolaan Kas Panitia Halal Bi Halal Dusun Ngrombo-Pepet</h3>
        </div>
    </div>              
    <!-- /. ROW  -->
    <hr />
    <div class="row">
        <div class="col-md-6 col-xs-12">           
            <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-green set-icon">
                    <i class="fa fa-briefcase"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text"><?php echo "Rp. ". number_format($output_kas_masuk).",-";  ?></p>
                    <p class="text-muted">Total Uang Kas Masuk</p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xs-12">           
            <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-red set-icon">
                    <i class="fa fa-shopping-cart"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text"><?php echo "Rp. ". number_format($output_kas_keluar).",-";  ?></p>
                    <p class="text-muted">Total Uang Kas Keluar</p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xs-12">           
            <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-blue set-icon">
                    <i class="fa fa-usd"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text"><?php echo "Rp. ". number_format($saldo).",-";  ?></p>
                    <p class="text-muted">Saldo Akhir Kas</p>
                </div>
            </div>
        </div>
    </div>
</div>
