<?php 
    error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
    require_once "core/init.php";

    if (!isset($_SESSION['user'])) {
        header('Location:login.php');
    }



    require_once "view/header.php";
    
?>



    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header ">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">EDIPENI</a> 
            </div>
        <div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;"> 
            Selamat datang <b style="text-transform:uppercase;"><?php echo $_SESSION['user']; ?></b> &nbsp; <a href="logout.php" class="btn btn-default square-btn-adjust">Logout</a> 
        </div>
        <div id="karya" class="text-center">
            <p>KARYA REMAJA NGROMBO-PEPET</p>
        </div>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<li class="text-center">
                    <img src="assets/img/edipeni.png" class="user-image img-responsive"/>
					</li>
				
					
                    <li>
                        <a  href="index.php"><i class="fa fa-home fa-3x"></i> DASHBOARD</a>
                    </li>
                      <li>
                        <a  href="?page=masuk"><i class="fa fa-briefcase fa-3x"></i> KAS MASUK</a>
                    </li>
                    <li>
                        <a  href="?page=keluar"><i class="fa fa-shopping-cart fa-3x"></i> KAS KELUAR</a>
                    </li>
					<li  >
                        <a  href="?page=rekap"><i class="fa fa-bar-chart fa-3x"></i> REKAPITULASI KAS</a>
                    </li>	
                      <li  >
                        <a  href="?page=user"><i class="fa fa-users fa-3x"></i> MANAJEMEN USER</a>
                    </li>
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     
                        <?php 
                            $page = $_GET["page"];
                            $aksi = $_GET["aksi"];

                            if ($page == "masuk") {
                                if ($aksi == "") {
                                    include "page/kas_masuk/masuk.php";                                    
                                }if ($aksi == "hapus") {
                                    include "page/kas_masuk/hapus.php";
                                }
                            }elseif ($page == "keluar") {
                                if ($aksi == "") {
                                    include "page/kas_keluar/keluar.php";
                                }if ($aksi == "hapus") {
                                    include "page/kas_keluar/hapus.php";
                                }
                            }elseif ($page == "rekap") {
                                if ($aksi == "") {
                                    include "page/kas_rekap/rekap.php";
                                }
                            }elseif ($page == "user") {
                                if ($aksi == "") {
                                    include "page/user/user.php";
                                }
                            }elseif ($page == "") {
                                    include "home.php";
                            }
                        ?>
                       
                    </div>
                </div> <!-- /. ROW  -->
                <hr>                   
                <p style="text-align: center; font-size: 15px;">&copy; Copyright by Deni</p>       
            </div> <!-- /. PAGE INNER  -->
        </div> <!-- /. PAGE WRAPPER  -->
    </div> <!-- /. WRAPPER  -->     

<?php require_once "view/footer.php"; ?>