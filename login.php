<?php 

  require_once "core/init.php";

  $error = '';

  if (isset($_SESSION['user'])) {
        header('Location:index.php');
  }

  if (isset($_POST['submit'])) {
    $nama = $_POST['username'];
    $pass = $_POST['password'];

    if (!empty(trim($nama)) && !empty(trim($pass))){
      if ( login_cek_nama($nama) ){
          if (cek_data($nama, $pass) ){
            $_SESSION['user'] = $nama;
            header('Location:index.php');
          }else{
            $error = "data ada yang salah";
          }
      }else{
        $error = "nama belum terdatar";
      }
    }else{
      $error = "ups tidak boleh kosong";
    }

  }

?>

<!DOCTYPE html>
<html>
<head>
  <title>LOGIN PAGE</title>
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
  <style type="text/css">
    *{
      font-family: 'Roboto', sans-serif;
      margin: 0;
      padding: 0;
    }
  </style>
</head>
<body style="background-color: #00bcd4 !important;">
    <div id="wrapper-log-in" style="background-color: #fff; width: 20%; margin: 0 auto; border-radius: 3px; margin-top: 80px;" >
      <div id="login-page" style="padding: 40px 20px;">
        <?php 
          // Menampilkan flash message
          if ( isset($_SESSION['msg']) ) { ?>
            <div id="flash_msg"> <?php flash_message_call($user); ?> </div>
        <?php } ?>
        <img src="assets/img/logo.png" style="width: 110px; height: 110px; display: block; margin: 0 auto; margin-bottom: 25px;">
        <p style="font-weight: 400; margin: 0; text-align: center;">MASUK DENGAN AKUN ANDA</p>
        <form action="login.php" method="post" style="margin-top: 30px;">
          <input type="text" name="username" style="width: 100%; padding: 5px; margin-top: 10px; border: none; border-bottom: 1px solid #00bcd4;" placeholder="Masukkan Nama"><br><br>
          <input type="password" name="password" style="width: 100%; padding: 5px; margin-top: 10px; border: none; border-bottom: 1px solid #00bcd4;" placeholder="Password" >
          <input type="submit" name="submit" value="MASUK" style="width: 100%; border: 0; padding: 8px; color: #fff; background-color: #ff4081; border-radius: 0; margin: 25px 0;">
        </form>        
        <a href="register.php" class="btn-register" style="color: #00bcd4;">Daftar Sekarang!</a>
        <?php if ( $error != '' ) { ?>
          <div id="error" style="background-color: red; color: #fff; padding: 5px 10px; margin-top: 5px;">
            <?= $error; ?>
          </div>
        <?php } ?>
      </div>
    </div>
</body>
</html>
